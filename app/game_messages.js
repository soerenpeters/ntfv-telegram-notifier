/**
 * Helper assertion function.
 * @param {boolean} condition
 * @param {string} message
 * @throws {Error} Will throw an Error if the condition is false.
 */
function assert(condition, message) {
  if (!condition) {
    message = message || 'Assertion failed';
    throw new Error(message);
  }
}

/**
 * Aquire the messages for updated games and send them to the passed callback.
 * @param {[Game]} oldGames
 * @param {[Game]} newGames
 * @param {[sendCallback]} sendMessages - list of callback
 * @return {boolean} Whether messages were sent or not.
 */
function sendMessages(oldGames, newGames, sendMessages) {
  const messages = getMessages(getUpdatedGames(oldGames, newGames));
  messages.forEach(function(message) {
    sendMessages.forEach(function(send) {
      send(message);
    });
  });
  return messages.length != 0;
}

/**
 * Compares two games based on the result and the goals.
 * @param {[Game]} game1
 * @param {[Game]} game2
 * @return {boolean} Whether games are equal or not
 */
function resultIsEqual(game1, game2) {
  return game1.goals == game2.goals && game1.result == game2.result;
}

/**
 * Returns a list of updated games. If not games are updated, the list is empty.
 * @param {[Game]} oldGames
 * @param {[Game]} newGames
 * @throws {Error} Will throw an Error if the game lists size is not equal.
 * @return {[Game]} Returns a list of updated games.
 */
function getUpdatedGames(oldGames, newGames) {
  assert(oldGames.length == newGames.length,
      'Old and new game list lengths have to be equal!');

  const updatedGames = [];
  oldGames.forEach(function(oldGame, index) {
    const newGame = newGames[index];

    if (!resultIsEqual(newGame, oldGame)) {
      updatedGames.push(newGame);
    }
  });
  return updatedGames;
}

/**
 * Returns a list of messages for the passed games.
 * @param {[Game]} games
 * @return {[string]} Returns a list of messages.
 */
function getMessages(games) {
  const messages = [];
  games.forEach((game) =>
    messages.push(getMessage(game)),
  );
  return messages;
}

/**
 * Build and return the message for a game.
 * @param {[Game]} game
 * @return {string} Returns the game message.
 */
function getMessage(game) {
  let message = 'Ergebnis Update:\n';
  message = message + game.date + '\n';
  message = message + game.team1 + ' vs. ' + game.team2 + '\n';
  message = message + 'Tore: ' + game.goals + '\n';
  message = message + 'Spielstand: ' + game.result + '\n';

  if (game.custom) {
    message = message + '(' + game.custom + ')\n';
  }

  message = message + game.link;

  return message;
}


// main entry function for the production
exports.sendMessages = sendMessages;

// exported for testing purpose
exports.getUpdatedGames = getUpdatedGames;
exports.getMessages = getMessages;
exports.getMessage = getMessage;
