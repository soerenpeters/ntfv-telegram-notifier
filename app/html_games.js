
const HTMLParser = require('node-html-parser');
const getHrefs = require('get-hrefs');

// eslint-disable-next-line no-unused-vars
const config = require('./../config/config.js');

/**
 * Check if index belogs to table. This functions is
 * used to skip table entries while parsing the html content.
 * @param {int} index
 * @return {boolean}
 */
function belongsToTable(index) {
  return index < (global.gConfig.tableEntriesToSkip / 2);
}

/**
 * Finds the url in the raw game string.
 * @param {string} entry
 * @return {string} url from game
 */
function getGameLink(entry) {
  const links = getHrefs(entry.toString());
  return links.filter(function(str) {
    // there are several links, 'veranstaltungid' indicates the right one.
    return str.includes('veranstaltungid');
  });
}

/**
 * Splits the raw game string into list of game entries.
 * Additionally removes "verantwortlich" from the game list.
 * @param {string} htmlEntry
 * @return {[string]} list of game entries
 */
function getGame(htmlEntry) {
  const game = htmlEntry.structuredText.split(/\r?\n/);
  // If 'verantwortlich' is added or removed
  // from an entry it should not trigger an update.
  // Therefore this entry is removed here.
  return game.filter((e) => !e.includes('verantwortlich'));
}

/**
 * Returns a list of games corresponding to the querySelector.
 * @param {HTMLElement} root
 * @param {string} querySelector
 * @return {[Object]}
 */
function getGames(root, querySelector) {
  const games = [];

  const sectionList = root.querySelectorAll(querySelector);
  sectionList.forEach(function(entry, index) {
    if (belongsToTable(index)) {
      return;
    }

    const gamelink = getGameLink(entry);
    const game = getGame(entry);

    games.push({
      date: game[0],
      team1: game[1],
      team2: game[2],
      goals: game[3],
      result: game[4],
      custom: game[5],
      link: 'https://ntfv.de' + gamelink,
    });
  });

  return games;
}

/**
 * Parse a list of games out of the raw html side.
 * @param {string} data - raw html side
 * @return {[Object]}
 */
function getGamesFromHTML(data) {
  const root = HTMLParser.parse(data);

  const games = getGames(root, 'tr.sectiontableentry1_tsl');
  games.push(...getGames(root, 'tr.sectiontableentry2_tsl'));

  return games;
}

exports.getGamesFromHTML = getGamesFromHTML;
