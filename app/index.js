
const https = require('https');
const fs = require('file-system');

const htmlGames = require('./html_games.js');
const gameMessages = require('./game_messages.js');
const sendMessages = require('./send_messages.js');

/**
 *
 * @param {*} games
 */
function writeGamesToJSON(games) {
  fs.writeFileSync('data/ntfv.json', JSON.stringify(games), 'utf-8');
}

/**
 * Start the process of
 * 1. reading the old json file
 * 2. parsing games of new html raw string
 * 3. sending messages for each new game
 * 4. overwrite json file with the new games
 * @param {string} data raw html string
 */
function parse(data) {
  fs.readFile('data/ntfv.json', 'utf-8', function(err, oldGamesRaw) {
    // get games from HTML
    const newGames = htmlGames.getGamesFromHTML(data);

    // if json file is not availabe: write file and return without error
    if (err && err.code == 'ENOENT') {
      console.log('data/ntfv.json not found.',
          'Write file and start next iteration...');
      writeGamesToJSON(newGames);
      return;
    } else if (err) {
      throw err;
    }

    // start comparison and write new json in case of new games
    const oldGames = JSON.parse(oldGamesRaw);
    if (gameMessages.sendMessages(
        oldGames,
        newGames,
        [sendMessages.sendToTelegram,
          sendMessages.sendToDiscord,
          sendMessages.sendToConsole])
    ) {
      writeGamesToJSON(newGames);
    }
    // else nothing changed
  });
}


/**
 * main entry
 */
function main() {
  // read ntfv html content
  https.get(global.gConfig.ntfv_url, (resp) => {
    let data = '';

    // A chunk of data has been received.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received.
    resp.on('end', () => {
      parse(data.toString()); // start parsing content
    });
  }).on('error', (err) => {
    console.log('Error while requesting the url from ntfv.de: ' + err.message);
  });
}

// initial call the main
main();

// and start the interval
setInterval(main, global.gConfig.interval_time_msec);
