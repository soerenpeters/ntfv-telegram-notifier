const Telegraf = require('telegraf');
const {WebhookClient} = require('discord.js');

/**
 * Send the passed message to the console.
 * @param {string} message
 */
function sendToConsole(message) {
  console.log(message);
}

/**
 * Send the passed message to the channel and the console.
 * @param {string} message
 */
function sendToTelegram(message) {
  const bot = new Telegraf(global.gTelegram.telegram_bot_api_key);
  bot.telegram.sendMessage(global.gTelegram.telegram_channel_id, message);
}

/**
 * Send the passed message to the discord channel.
 * @param {string} message
 */
function sendToDiscord(message) {
  const webhookClient = new WebhookClient({
    id: global.gDiscord.discord_webhook_id,
    token: global.gDiscord.discord_webhook_token,
  });

  // const embed = new EmbedBuilder()
  //   .setTitle('Some Title')
  //   .setColor(0x00FFFF);

  webhookClient.send({
    content: message,
    username: 'Braunschweig Liga',
    avatarURL: 'https://upload.wikimedia.org/wikipedia/de/thumb/5/53/Eidenbenzl%C3%B6we.svg/170px-Eidenbenzl%C3%B6we.svg.png',
    // embeds: [embed],
  });
}

// exports
exports.sendToConsole = sendToConsole;
exports.sendToTelegram = sendToTelegram;
exports.sendToDiscord = sendToDiscord;
