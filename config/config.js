// https://codeburst.io/node-js-best-practices-smarter-ways-to-manage-config-files-and-variables-893eef56cbef

// requires
const _ = require('lodash');
const fs = require('fs');

// read config
const config = require('./config.json');
const defaultConfig = config.development;
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
const finalConfig = _.merge(defaultConfig, environmentConfig);

// as a best practice
// all global variables should be referenced via global. syntax
// and their names should always begin with g
global.gConfig = finalConfig;

console.log(`global.gConfig: ${JSON.stringify(
    global.gConfig,
    undefined,
    global.gConfig.json_indentation)}`,
);

// read telgram api keys
fs.stat('config/telegram_api.json', function(err, stat) {
  if (err == null) {
    // read api keys to config
    const apiConfig = require('./telegram_api.json');
    const apiDefaultConfig = apiConfig.development;
    const apiEnvironmentConfig = apiConfig[environment];
    const apiFinalConfig = _.merge(apiDefaultConfig, apiEnvironmentConfig);
    global.gTelegram = apiFinalConfig;

    console.log(`global.gTelegram: ${JSON.stringify(
        global.gTelegram,
        undefined,
        global.gTelegram.json_indentation)}`,
    );
  } else if (err.code === 'ENOENT') {
    // file not exists
    console.log('config/telegram_api.json not found.');
  } else {
    console.log('Error reading config/telegram_api.json: ', err.code);
  }
});


// read discord webhook
fs.stat('config/discord_webhook.json', function(err, stat) {
  if (err == null) {
    // read api keys to config
    const apiConfig = require('./discord_webhook.json');
    const apiDefaultConfig = apiConfig.development;
    const apiEnvironmentConfig = apiConfig[environment];
    const apiFinalConfig = _.merge(apiDefaultConfig, apiEnvironmentConfig);
    global.gDiscord = apiFinalConfig;

    console.log(`global.gDiscord: ${JSON.stringify(
        global.gDiscord,
        undefined,
        global.gDiscord.json_indentation)}`,
    );
  } else if (err.code === 'ENOENT') {
    // file not exists
    console.log('config/discord_webhook.json not found.');
  } else {
    console.log('Error reading config/discord_webhook.json: ', err.code);
  }
});

