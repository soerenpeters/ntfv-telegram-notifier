const assert = require('assert');

const gameMessages = require('../app/game_messages.js');

describe('Build the send message.', () => {
  let game;

  beforeEach(function() {
    game = {
      'date': 'Montag',
      'team1': 'Team1',
      'team2': 'Team2',
      'goals': '10:10',
      'result': '1:1',
      'link': 'https://mylink.de',
    };
  });

  /**
   *
   * @return {string} standard random game message
   */
  function getGameMessage() {
    return 'Ergebnis Update:\nMontag\nTeam1 vs. Team2\nTore: 10:10\nSpielstand: 1:1\nhttps://mylink.de';
  }

  /**
   *
   * @return {string} standard random game message with live identifier
   */
  function getLiveGameMessage() {
    return 'Ergebnis Update:\nMontag\nTeam1 vs. Team2\nTore: 10:10\nSpielstand: 1:1\n(live)\nhttps://mylink.de';
  }

  it('should return the telegram message from game', () => {
    assert.equal(getGameMessage(), gameMessages.getMessage(game));
  });

  it('should return the telegram message from live game', () => {
    game.custom = 'live';

    assert.equal(getLiveGameMessage(), gameMessages.getMessage(game));
  });
});


describe('Get the new game list.', () => {
  const game1 = {
    'date': 'Montag',
    'team1': 'Team1',
    'team2': 'Team2',
    'goals': '10:10',
    'result': '1:1',
    'link': 'https://mylink1.de',
  };

  const game2 = {
    'date': 'Dienstag',
    'team1': 'Team3',
    'team2': 'Team4',
    'goals': '3:4',
    'result': '0:2',
    'link': 'https://mylink2.de',
  };

  let oldGames = [];
  let newGames = [];

  beforeEach(function() {
    oldGames = [];
    newGames = [];
    oldGames.push(game1);
    oldGames.push(game2);
  });


  it('should throw an Error, if game list length are not equal', () => {
    assert.throws(function() {
      gameMessages.getUpdatedGames(oldGames, newGames);
    }, Error);
  });

  it('should return no new games, if they are all equal', () => {
    newGames.push(game1);
    newGames.push(game2);

    assert.deepEqual(gameMessages.getUpdatedGames(oldGames, newGames), []);
  });

  it('should return the new game, if something changed', () => {
    newGames.push(game1);
    updatedGame2 = Object.assign({}, game2);
    updatedGame2.goals = '99:99';
    newGames.push(updatedGame2);

    assert.deepEqual(
        gameMessages.getUpdatedGames(oldGames, newGames),
        [updatedGame2],
    );
  });

  it('should return all new game, if something changed', () => {
    updatedGame1 = Object.assign({}, game1);
    updatedGame1.goals = '88:88';
    updatedGame2 = Object.assign({}, game2);
    updatedGame2.goals = '99:99';

    newGames.push(updatedGame1);
    newGames.push(updatedGame2);

    assert.deepEqual(
        gameMessages.getUpdatedGames(oldGames, newGames),
        [updatedGame1, updatedGame2],
    );
  });

  it('should not return a new game, if only the date changes', () => {
    updatedGame1 = Object.assign({}, game1);
    updatedGame2 = Object.assign({}, game2);
    updatedGame2.date = 'New Date';

    newGames.push(updatedGame1);
    newGames.push(updatedGame2);

    assert.deepEqual(gameMessages.getUpdatedGames(oldGames, newGames), []);
  });


  it('shouldSendTheMessage', () => {
    updatedGame1 = Object.assign({}, game1);
    updatedGame1.goals = '88:88';
    newGames.push(updatedGame1);
    newGames.push(game2);

    const expectedMessage = 'Ergebnis Update:\nMontag\nTeam1 vs. Team2\nTore: 88:88\nSpielstand: 1:1\nhttps://mylink1.de';

    gameMessages.sendMessages(oldGames, newGames, [function(message) {
      assert.equal(message, expectedMessage);
    }]);
  });
});


describe('Get from game list the messages.', () => {
  it('should return no message, if no games are passed', () => {
    assert.deepEqual(gameMessages.getMessages([]), []);
  });

  it('should return message for a game', () => {
    const game = {
      'date': 'Montag',
      'team1': 'Team1',
      'team2': 'Team2',
      'goals': '10:10',
      'result': '1:1',
      'link': 'https://mylink.de',
    };

    const message = 'Ergebnis Update:\nMontag\nTeam1 vs. Team2\nTore: 10:10\nSpielstand: 1:1\nhttps://mylink.de';

    assert.deepEqual(gameMessages.getMessages([game]), [message]);
  });

  it('should return all message for game list', () => {
    const game1 = {
      'date': 'Montag',
      'team1': 'Team1',
      'team2': 'Team2',
      'goals': '10:10',
      'result': '1:1',
      'link': 'https://mylink.de',
    };

    const game2 = Object.assign({}, game1);
    game2.goals = '20:20';

    const message1 = 'Ergebnis Update:\nMontag\nTeam1 vs. Team2\nTore: 10:10\nSpielstand: 1:1\nhttps://mylink.de';
    const message2 = 'Ergebnis Update:\nMontag\nTeam1 vs. Team2\nTore: 20:20\nSpielstand: 1:1\nhttps://mylink.de';

    assert.deepEqual(
        gameMessages.getMessages([game1, game2]),
        [message1, message2],
    );
  });
});
