const assert = require('assert');
const fs = require('file-system');

const htmlGames = require('../app/html_games.js');

describe('Parsing the html file.', () => {
  let data = '';

  beforeEach(function() {
    data = fs.readFileSync('test/games_test.html', 'utf-8');
  });

  it('should skip the table and returning the given games from the html file',
      () => {
        const games = htmlGames.getGamesFromHTML(data);
        assert.equal(games.length, 2);
      });
});
